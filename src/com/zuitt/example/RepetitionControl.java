package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {
    public static void main(String[] args){
        //Loops ------------------------------>

        //While Loop
        int x = 0;
        while (x < 5){
            System.out.println("While Loop: " + x);
            x++;
        }

        //Do-While Loop
        int y = 10;
        do {
            System.out.println("Countdown: " + y);
            y--;
        }while (y > 10);

        //For Loop
        for(int i = 0; i < 5; i++){
            System.out.println("Current Count: " + i );
        }

        //For Loop with Arrays 1st Method
        int [] intArray = {100, 200, 300, 400, 500};
        for(int i = 0; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }

        //For Loop with Arrays 2nd Method
        String[] nameArray = {"John", "Paul", "George", "Ringo"};
        for(String name : nameArray){
            System.out.println(name);
        }

        //Nested Loops for Multidimensional Arrays
        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //Outer Loop
        for(int row = 0; row < 3; row++){
            //Inner Loop
            for (int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }

        //For Each with MultiDimensional Array
        for(String[] row: classroom){
            for(String column: row){
                System.out.println(column);
            }
        }

        //For Each with ArrayList
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);

        numbers.forEach(num -> System.out.println("ArrayList of Numbers: " + num));


        //For Each with HashMaps
        HashMap<String, Integer> grades = new HashMap<>()
        {{
            put("English", 90);
            put("Math", 95);
            put("Science", 97);
            put("History", 94);
        }};

        System.out.println(grades);
        grades.forEach((subject, grade)->System.out.println("The student grade for " + subject + " is " + grade));

    }
}
