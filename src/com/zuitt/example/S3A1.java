package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void main(String[] args){
        System.out.println("Input an integer whose factorial will be computed:");
        Scanner in = new Scanner(System.in);
        int num = 0;
        int answer = 1;
        int counter = 1;

        try{
            num = in.nextInt();
        }
        catch (Exception e){
            System.out.println("Invalid Input!");
            e.printStackTrace();
        }
        finally {
            int originalNum = num;

            if (originalNum < 1){
                System.out.println("Please enter a number greater than zero.");
            }else{
                while (counter < originalNum){
                    if (counter == 1){
                        answer = num*(num-1);
                    }else {
                        answer = answer*(num-1);
                    }
                    num--;
                    counter++;
                }
                System.out.println("The factorial of " + originalNum + " is " + answer);
            }
        }

    }
}
